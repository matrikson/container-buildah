FROM fedora:latest
RUN dnf -y --setopt=fastestmirror=true upgrade && \
    dnf -y install podman buildah && \
    dnf clean all
